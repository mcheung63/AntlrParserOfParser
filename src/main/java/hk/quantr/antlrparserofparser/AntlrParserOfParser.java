//package hk.quantr.antlrparserofparser;
//
//import hk.quantr.antlrparserofparser.antlr.ANTLRv4Lexer;
//import hk.quantr.antlrparserofparser.antlr.ANTLRv4Parser;
//import java.io.IOException;
//import java.net.URL;
//import org.antlr.v4.runtime.CharStreams;
//import org.antlr.v4.runtime.CommonTokenStream;
//import org.antlr.v4.runtime.RecognitionException;
//import org.antlr.v4.runtime.tree.ParseTreeWalker;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
//public class AntlrParserOfParser {
//
//	public static Logger logger = Logger.getLogger(AntlrParserOfParser.class);
//
//	public static void main(String args[]) {
//		try {
//			ANTLRv4Lexer lexer = new ANTLRv4Lexer(CharStreams.fromStream(new URL("https://raw.githubusercontent.com/antlr/grammars-v4/master/antlr4/ANTLRv4Parser.g4").openStream()));
//			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
//			ANTLRv4Parser parser = new ANTLRv4Parser(tokenStream);
//			ANTLRv4Parser.GrammarSpecContext asmContext = parser.grammarSpec();
//
//			ParseTreeWalker walker = new ParseTreeWalker();
//			MyAntlrListener listener = new MyAntlrListener();
//			walker.walk(listener, asmContext);
//		} catch (RecognitionException ex) {
//			logger.error(ex.getMessage());
//		} catch (IOException ex) {
//			logger.error(ex.getMessage());
//		}
//	}
//}
