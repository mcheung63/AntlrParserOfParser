//package hk.quantr.antlrparserofparser;
//
//import hk.quantr.antlrparserofparser.antlr.ANTLRv4Parser;
//import hk.quantr.antlrparserofparser.antlr.ANTLRv4ParserBaseListener;
//import org.antlr.v4.runtime.ParserRuleContext;
//import org.apache.log4j.Logger;
//
///**
// *
// * @author Peter <peter@quantr.hk>
// */
//public class MyAntlrListener extends ANTLRv4ParserBaseListener {
//
//	public static Logger logger = Logger.getLogger(MyAntlrListener.class);
//
//	@Override
//	public void enterGrammarSpec(ANTLRv4Parser.GrammarSpecContext ctx) {
////		logger.info(">" + ctx.getText());
//	}
//
//	@Override
//	public void exitEveryRule(ParserRuleContext ctx) {
//		logger.info(">" + ctx.getText());
//	}
//}
