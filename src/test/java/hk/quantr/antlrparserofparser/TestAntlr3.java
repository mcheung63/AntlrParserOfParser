package hk.quantr.antlrparserofparser;

import hk.quantr.antlrparserofparser.antlr.ANTLRv3BaseListener;
import hk.quantr.antlrparserofparser.antlr.ANTLRv3Lexer;
import hk.quantr.antlrparserofparser.antlr.ANTLRv3Parser;
import java.util.BitSet;
import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class TestAntlr3 {

	@Test
	public void testAntlr3() {
		try {
			ANTLRv3Lexer lexer = new ANTLRv3Lexer(new ANTLRInputStream(IOUtils.toString(getClass().getResourceAsStream("/csv.g"), "utf-8")));
//			CommonTokenStream tokenStream = new CommonTokenStream(lexer);
			Token token;
			while ((token = lexer.nextToken()).getType() != ANTLRv3Lexer.EOF) {
				System.out.println(token + "\t= " + token.getType() + "\t - \t" + ANTLRv3Lexer.VOCABULARY.getSymbolicName(token.getType()));
//				System.out.println(ANTLRv3Lexer.VOCABULARY.getDisplayName(token.getType()));
//				System.out.println(ANTLRv3Lexer.VOCABULARY.getLiteralName(token.getType()));
			}
//			ANTLRv3Parser parser = new ANTLRv3Parser(tokenStream);
//			parser.addErrorListener(new ANTLRErrorListener() {
//				@Override
//				public void syntaxError(Recognizer<?, ?> rcgnzr, Object offendingSymbol, int lineNumber, int charOffsetFromLine, String message, RecognitionException re) {
//					Token offendingToken = (Token) offendingSymbol;
//					int start = offendingToken.getStartIndex();
//					int stop = offendingToken.getStopIndex();
////					System.out.println("syntaxError " + rcgnzr + ", " + lineNumber + ", " + charOffsetFromLine + ", " + start + ", " + stop + ", " + message + ", " + re);
//					System.out.println("ERROR " + message);
//
//				}
//
//				@Override
//				public void reportAmbiguity(org.antlr.v4.runtime.Parser parser, DFA dfa, int i, int i1, boolean bln, BitSet bitset, ATNConfigSet atncs) {
//					System.out.println("reportAmbiguity");
//				}
//
//				@Override
//				public void reportAttemptingFullContext(org.antlr.v4.runtime.Parser parser, DFA dfa, int i, int i1, BitSet bitset, ATNConfigSet atncs) {
//					System.out.println("reportAttemptingFullContext");
//				}
//
//				@Override
//				public void reportContextSensitivity(org.antlr.v4.runtime.Parser parser, DFA dfa, int i, int i1, int i2, ATNConfigSet atncs) {
//					System.out.println("reportContextSensitivity");
//				}
//			});
//			ANTLRv3Parser.GrammarDefContext context = parser.grammarDef();
//			ParseTreeWalker walker = new ParseTreeWalker();
//			MyANTLRv3ParserListener listener = new MyANTLRv3ParserListener();
//			walker.walk(listener, context);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	class MyANTLRv3ParserListener extends ANTLRv3BaseListener {

		@Override
		public void enterEveryRule(ParserRuleContext ctx) {
//			System.out.println(ctx.getText());
		}
	}
}
